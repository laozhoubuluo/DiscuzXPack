<?php

function check_necessary_extension()
{
    $needed = array('curl', 'mbstring');
    $not_loaded = '';
    foreach ($needed as $ext) {
        if (!extension_loaded($ext)) {
            $not_loaded .= $ext . ' ';
        }
    }
    if ($not_loaded) {
        logging\error("cannot find extension %s", $not_loaded);
        exit(1);
    }

    global $G_HAS_ZIP_COMMAND;
    logging\info("trying to find zip and unzip command");
    exec('zip -L', $output, $res);
    if ($res != 0) {
        logging\warning("cannot find zip command");
        $G_HAS_ZIP_COMMAND = false;
    }
    exec('unzip -Z', $output, $res);
    if ($res != 0) {
        logging\warning("cannot find unzip command");
        $G_HAS_ZIP_COMMAND = false;
    }
    if (!$G_HAS_ZIP_COMMAND) {
        if (!extension_loaded('zip')) {
            logging\error("cannot find zip extension");
            exit(1);
        }
    }
}

function rrmdir($path)
{
    logging\info("removing folder %s", $path);
    if (PHP_OS === 'Windows' || PHP_OS === 'WINNT') {
        exec(sprintf("rd /s/q \"%s\"", $path));
    } else {
        exec(sprintf("rm -rf %s", $path));
    }
}

function try_mkdir($path, $mode = 0777, $recursive = false)
{
    if (file_exists($path)) return;
    mkdir($path, $mode, $recursive);
}

function cpdir($from, $to)
{
    try_mkdir($to, 0777, true);
    logging\info("copying folder %s -> %s", $from, $to);
    if (PHP_OS === 'Windows' || PHP_OS === 'WINNT') {
        exec(sprintf("xcopy /s/y/q \"%s\" \"%s\"", $from, $to));
    } else {
        exec(sprintf("cp -r %s/* %s", $from, $to));
    }
}

function rmfile($path)
{
    logging\info("removing file %s", $path);
    unlink($path);
}

function mkdir_of_file($file)
{
    try_mkdir(dirname($file), 0777, true);
}

function trimds($s)
{
    return rtrim($s, '/');
}

function join_paths()
{
    return implode('/', array_map('trimds', func_get_args()));
}

function str_contains($haystack, $needle)
{
    return strpos($haystack, $needle) !== FALSE;
}

function starts_with($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function ends_with($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

function detect_eol($str)
{
    static $eols = array(
        "\n\r",  // 0x0A - 0x0D - acorn BBC
        "\r\n",  // 0x0D - 0x0A - Windows, DOS OS/2
        "\n",    // 0x0A -      - Unix, OSX
        "\r",    // 0x0D -      - Apple ][, TRS80
    );

    $curCount = 0;
    $curEol = '';
    foreach ($eols as $eol) {
        if (($count = substr_count($str, $eol)) > $curCount) {
            $curCount = $count;
            $curEol = $eol;
        }
    }
    return $curEol;
}

function get_release_date()
{
    return date('YmdHi', time());
}

function get_hash_from_revision($str) {
    $a = explode("-", $str);
    return end($a);
}

function check_if_already_done($zipdir) {
    process_all_files_with_condition($zipdir, "", 
        function ($fi) {
            return strtolower($fi->getExtension()) === 'zip';
        }, 
        function ($from_file, $to_file) {
            global $G_RELEASE_REVISION;
            $hash = get_hash_from_revision($G_RELEASE_REVISION);
            if (strpos($from_file, $hash) !== FALSE) {
                logging\info("this revision has been packed, exiting.");
                exit(0);
            }
        });
}

function check_usage($argv) {
    if (count($argv) < 4) {
        echo "php main.php <branch> <temp working dir> <zip output dir> --clean --skip-download --skip-unzip --skip-package --skip-zip --skip-cleanup";
        exit(1);
    }
}