<?php

require_once "vendor/steelywing/Chinese.php";

/*
  处理所有文件的通用函数
  $from_path: 要遍历的目录
  $to_path: 目标目录，会与源目录下文件的相对路径一起拼起来作为参数传给$process_func
  $condition: 条件函数，传入参数是遍历目录下的每一个fileInfo，返回值如果为true，则执行$process_func
  $process_func: 处理函数，传入源文件和目标文件
 */
function process_all_files_with_condition($from_path, $to_path, $condition, $process_func)
{
    if (!file_exists($from_path)) return;

    $dir = new DirectoryIterator($from_path);
    foreach ($dir as $fi) {
        if (!$fi->isDot()) {
            $filename = $fi->getFilename();
            if ($fi->isDir()) {
                process_all_files_with_condition(join_paths($from_path, $filename), join_paths($to_path, $filename), $condition, $process_func);
            } else {
                if ($condition($fi)) {
                    $process_func(join_paths($from_path, $filename), join_paths($to_path, $filename));
                }
            }
        }
    }
}


// -------------------------------------------------------------------------------------------------------------------------------------------------
// 复制一个文件，工具函数
function copy_file($from_file, $to_file)
{
    mkdir_of_file($to_file);
    copy($from_file, $to_file);
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// 复制所有非js, 非php文件
function copy_all_other_files($from_path, $to_path)
{
    logging\info("copying all files other than .js and .php");
    process_all_files_with_condition(
        $from_path,
        $to_path,
        function ($fi) {
            $ext = strtolower($fi->getExtension());
            return !in_array($ext, array('js', 'php'));
        },
        'copy_file'
    );
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// 复制所有文件
function copy_all_files($from_path, $to_path)
{
    logging\info("copying files while keeping folder structure %s -> %s", $from_path, $to_path);
    process_all_files_with_condition(
        $from_path,
        $to_path,
        function ($fi) {
            return true;
        },
        'copy_file'
    );
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// 以下两个函数用于原地转编码(charset)，源编码固定为UTF8
function encoding_condition($fi)
{
    if (str_contains($fi->getPath(), "/template/default/m/")) return false;
    if (ends_with($fi->getFilename(), 'lang_swfupload.php')) return false;

    $ext = strtolower($fi->getExtension());
    if (in_array($ext, array('md', 'css', 'js', 'php', 'xml', 'htm', 'ini', 'txt', 'sql'))) {
        return true;
    }
    return false;
}

function convert_encoding_in($from_path, $to_encoding)
{
    logging\info("convert encoding %s : %s", $to_encoding, $from_path);
    process_all_files_with_condition(
        $from_path,
        $from_path,
        'encoding_condition',
        function ($from_file, $to_file) use (&$to_encoding) {
            $buffer = file_get_contents($from_file);
            $buffer = mb_convert_encoding($buffer, $to_encoding, "UTF-8");
            file_put_contents($to_file, $buffer);
        }
    );
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// 以下函数用于转换语言到繁体，要求输入为utf8编码

$chinese = new SteelyWing\Chinese\Chinese(array('zh-Hant', 'zh-TW'));

function convert_to_hant($from_path)
{
    global $chinese;

    logging\info("convert language to taiwan-language: %s", $from_path);
    process_all_files_with_condition(
        $from_path,
        $from_path,
        function ($fi) {
            $ext = strtolower($fi->getExtension());
            return in_array($ext, array('md', 'css', 'js', 'php', 'xml', 'htm', 'ini', 'txt', 'sql'));
        },
        function ($from_file, $to_file) use (&$chinese) {
            $buffer = file_get_contents($from_file);
            $buffer = preg_replace('/simsun/ismU', 'mingliu', $buffer);
            $buffer = preg_replace('/宋体/ismU', '细明体', $buffer);
            $buffer = preg_replace('/楷体_GB2312/ismU', '标楷体', $buffer);
            $buffer = $chinese->to('zh-Hant', $buffer);
            $buffer = $chinese->to('zh-TW', $buffer);
            file_put_contents($to_file, $buffer);
        }
    );
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// 以下函数用于替换各文件中的encoding, charset变量

function replace_encoding_variables($from_path, $encoding)
{
    $charset = 'utf-8';
    $dbcharset = 'utf8';
    $language = 'zh_cn';
    if (!ends_with($encoding, 'UTF8')) {
        $strs = explode("_", $encoding);
        $charset = $dbcharset = strtolower($strs[1]);
    }
    if (starts_with($encoding, 'TC_')) {
        $language = 'zh_tw';
    }

    process_all_files_with_condition(
        $from_path,
        $from_path,
        function ($fi) {
            return in_array($fi->getFilename(), array(
                'config_global_default.php',
                'config_ucenter_default.php',
                'install_var.php',
                'var.inc.php'
            ));
        },
        function ($from_file, $to_file) use (&$encoding, &$charset, &$dbcharset, &$language) {
            logging\info("replacing encoding and language variables: %s", $from_file);

            $buffer = file_get_contents($from_file);
            $buffer = preg_replace('/(define\(\'INSTALL_LANG\', \').*(\'.*)/ismU', '${1}' . $encoding . '${2}', $buffer);
            $buffer = preg_replace('/(define\(\'CHARSET\', \').*(\'.*)/ismU', '${1}' . $charset . '${2}', $buffer);
            $buffer = preg_replace('/(define\(\'DBCHARSET\', \').*(\'.*)/ismU', '${1}' . $dbcharset . '${2}', $buffer);
            $buffer = preg_replace('/(define\(\'UC_CHARSET\', \').*(\'.*)/ismU', '${1}' . $charset . '${2}', $buffer);
            $buffer = preg_replace('/(define\(\'UC_DBCHARSET\', \').*(\'.*)/ismU', '${1}' . $dbcharset . '${2}', $buffer);
            $buffer = preg_replace('/(\$_config\[\'output\'\]\[\'charset\'\]\s*=\s*\').*(\'.*)/ismU', '${1}' . $charset . '${2}', $buffer);
            $buffer = preg_replace('/(\$_config\[\'db\'\]\[\'?1\'?\]\[\'dbcharset\'\]\s*=\s*\').*(\'.*)/ismU', '${1}' . $dbcharset . '${2}', $buffer);
            $buffer = preg_replace('/(\$_config\[\'output\'\]\[\'language\'\]\s*=\s*\').*(\'.*)/ismU', '${1}' . $language . '${2}', $buffer);

            file_put_contents($to_file, $buffer);
        }
    );
}

// -------------------------------------------------------------------------------------------------------------------------------------------------
// 以下函数用于生成版本号，修复admincp_checktools

function fix_files_misc($from_path)
{
    process_all_files_with_condition(
        $from_path,
        $from_path,
        function ($fi) {
            return in_array($fi->getFilename(), array(
                'discuz_version.php'
            ));
        },
        function ($from_file, $to_file) {
            global $G_RELEASE_REVISION;

            $buffer = file_get_contents($from_file);

            if (ends_with($from_file, 'discuz_version.php')) {
                logging\info("updating revision %s", $from_file);
                $buffer = preg_replace('/(define\(\'DISCUZ_RELEASE\', \').*(\'.*)/ismU', '${1}' . $G_RELEASE_REVISION . '${2}', $buffer);
            }
            
            file_put_contents($to_file, $buffer);
        }
    );
}
