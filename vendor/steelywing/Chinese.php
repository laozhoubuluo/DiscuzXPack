<?php

namespace SteelyWing\Chinese;


class Chinese
{
    const ZH_HANS = 'zh-Hans';
    const ZH_HANT = 'zh-Hant';
    const CHS = self::ZH_HANS;
    const CHT = self::ZH_HANT;
    public $dictPath;
    private $locale = self::ZH_HANT;
    private $dict = array();

    public function __construct(array $dictionaries = array(self::ZH_HANS, self::ZH_HANT))
    {
        $this->dictPath = __DIR__ . '/dict/';
        foreach ($dictionaries as $dict) {
            $this->load($dict);
        }
    }

    public function load($locale, $path = null)
    {
        if ($path === null) {
            $path = $this->dictPath . $locale . '.csv';
        }

        $originalLocale = array(
            'LC_COLLATE' => setlocale(LC_COLLATE, 0),
            'LC_CTYPE' => setlocale(LC_CTYPE, 0),
        );

        // If not set to zh_*, parsing CSV will not work properly
        // setlocale(LC_ALL, 'zh_Hant');
        setlocale(LC_COLLATE, 'zh_Hant');
        setlocale(LC_CTYPE, 'zh_Hant');

        $file = new \SplFileObject($path);
        $file->setFlags(
            \SplFileObject::SKIP_EMPTY |
            \SplFileObject::READ_AHEAD
        );

        $dict = array();
        $this->dict[$locale] =& $dict;
        while (!$file->eof()) {
            $fields = preg_split('/,/u', rtrim($file->fgets()));
            if (count($fields) != 2) {
                echo(sprintf("WARNING: Cannot parse '%s' file (line %s)\n",
                        $file->getPathname(),
                        $file->key()
                    )
                );
                continue;
            }
            $dict[$fields[0]] = $fields[1];
        }

        setlocale(LC_COLLATE, $originalLocale['LC_COLLATE']);
        setlocale(LC_CTYPE, $originalLocale['LC_CTYPE']);

        return $this;
    }

    public function to($locale, $string)
    {
        return strtr($string, $this->dict[$locale]);
    }

    /**
     * @param $string
     * @return string
     */
    public function convert($string)
    {
        return strtr($string, $this->dict[$this->locale]);
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        if (!isset($this->dict[$locale])) {
            throw new \InvalidArgumentException("Locale not found: {$locale}");
        }
        $this->locale = $locale;
    }
}
